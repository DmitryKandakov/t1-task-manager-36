package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.dkandakov.tm.api.repository.IProjectRepository;
import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.api.repository.IUserRepository;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.api.service.IUserService;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.field.IdEmptyException;
import ru.t1.dkandakov.tm.exception.user.EmailEmptyException;
import ru.t1.dkandakov.tm.exception.user.LoginEmptyException;
import ru.t1.dkandakov.tm.exception.user.PasswordEmptyException;
import ru.t1.dkandakov.tm.exception.user.RoleEmptyException;
import ru.t1.dkandakov.tm.model.User;
import ru.t1.dkandakov.tm.repository.ProjectRepository;
import ru.t1.dkandakov.tm.repository.TaskRepository;
import ru.t1.dkandakov.tm.repository.UserRepository;

import java.util.Random;
import java.util.UUID;

public final class UserRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull
    private final String userLogin = UUID.randomUUID().toString();

    @NotNull
    private final String userPassword = UUID.randomUUID().toString();

    @NotNull
    private final String userEmail = UUID.randomUUID().toString();

    @NotNull
    private final Random random = new Random();

    @Test
    public void create() {
        @Nullable final String emptyEmail = null;
        @Nullable final Role emptyRole = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(userLogin, userPassword, emptyEmail));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(userLogin, userPassword, emptyRole));
        Assert.assertEquals(0, userService.getSize());
        final int repositorySize = 3;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), userPassword);
        }
        Assert.assertEquals(repositorySize, userService.getSize());
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertEquals(user, userService.findByLogin(userLogin));

    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
        Assert.assertEquals(user, userService.findByEmail(userEmail));

    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), userPassword, String.format("User_Email_%d", i));
        }
        Assert.assertEquals(repositorySize, userService.getSize());
        final int randInt = random.nextInt(4);
        @NotNull final String login = "User_LoginNumber_" + randInt;
        userService.removeByLogin(login);
        Assert.assertEquals(repositorySize - 1, userService.getSize());
        Assert.assertFalse(userService.isLoginExist(login));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword,
                    String.format("TestEmail_%d", i)
            );
        }
        Assert.assertEquals(usersLength, userService.getSize());
        final int randInt = random.nextInt(10);
        @NotNull final String userEmail = "TestEmail_" + randInt;
        userService.removeByEmail(userEmail);
        Assert.assertEquals(usersLength - 1, userService.getSize());
        Assert.assertFalse(userService.isEmailExist(userEmail));
    }

    @Test
    public void setPassword() {
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, userPassword));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword("", userPassword));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), ""));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), null));
        Assert.assertEquals(user.getPasswordHash(), userService.findByLogin(userLogin).getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull String newId = UUID.randomUUID().toString();
        @NotNull String newFirstName = UUID.randomUUID().toString();
        @NotNull String newLastName = UUID.randomUUID().toString();
        @NotNull String newMiddleName = UUID.randomUUID().toString();
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> userService.updateUser(newId, newFirstName, newLastName, newMiddleName)
        );
        @Nullable User user = userService.create(userLogin, userPassword);
        @Nullable User updatedUser = userService.updateUser(user.getId(), newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(user, updatedUser);
        Assert.assertEquals(newFirstName, updatedUser.getFirstName());
        Assert.assertEquals(newLastName, updatedUser.getLastName());
        Assert.assertEquals(newMiddleName, updatedUser.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword
            );
        }
        final int randInt = random.nextInt(10);
        @NotNull final String userLogin = "TestLogin_" + randInt;
        Assert.assertTrue(userService.isLoginExist(userLogin));
    }

    @Test
    public void isMailExist() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isEmailExist(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isEmailExist(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword,
                    String.format("TestMail_%d", i)
            );
        }
        final int randInt = random.nextInt(10);
        @NotNull final String userMail = "TestMail_" + randInt;
        Assert.assertTrue(userService.isEmailExist(userMail));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.getLocked());
    }

}

