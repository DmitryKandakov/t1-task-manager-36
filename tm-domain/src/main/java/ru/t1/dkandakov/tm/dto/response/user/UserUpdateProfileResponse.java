package ru.t1.dkandakov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.response.AbstractUserResponse;
import ru.t1.dkandakov.tm.model.User;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

}
