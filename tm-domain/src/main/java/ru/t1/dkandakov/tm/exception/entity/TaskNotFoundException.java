package ru.t1.dkandakov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Error! Task wasn't found...");
    }

}
